<?php

set_time_limit(120);

// load ext libraries
require_once('../vendor/autoload.php');

header('Content-Type: application/json');

if (!isset($_GET['token']))
{
	echo '{"error":"invalid token"}';
	exit();
}

// store studydebt
if (isset($_GET['action']) && $_GET['action'] == 'store')
{
	store($_GET['token'], $_POST['studydebt']);
	echo '{"message":"ok"}';
	exit();
}

// get the studydebt and send it in encrypted form to the user
if (isset($_GET['action']) && $_GET['action'] == 'retrieve')
{
	// get public key
	$Pubkey = OpenPGP_Message::parse(OpenPGP::unarmor($_POST['publickey']));

	$studydebt = retrieve($_GET['token']);
	
	if ($studydebt === false)
	{
		echo '{"error":"invalid token"}';
		exit();
	}

	$data = new OpenPGP_LiteralDataPacket($studydebt, array('format' => 'u', 'filename' => 'studydebt.txt'));
	
	
	$encrypted = OpenPGP_Crypt_Symmetric::encrypt($Pubkey, new OpenPGP_Message(array($data)));
	
	// armor encrypted message to be able to transport it
	$enc = OpenPGP::enarmor($encrypted->to_bytes(), "PGP MESSAGE");
	$enc =  wordwrap($enc, 64, "\n", 1);	
	
	//print_r ($enc);
	echo '{"payload":'.json_encode($enc).'}';
	exit();
		
	
}

echo '{"error":"invalid action"}';
exit();




function store($token, $value)
{
	$filename = dirname(__FILE__) . '/../store/' . $token . '.txt';
	$fp = fopen($filename, 'w+');
	fwrite($fp, $value);
	fclose($fp);	
}

function retrieve($token)
{
	$filename = dirname(__FILE__) . '/../store/' . $token . '.txt';
	
	if (!file_exists($filename))
		return false;
	
	return file_get_contents($filename);	
}