# PoC Volksbank DUO
Volksbank and DUO mockup sites supporting the demonstration flow for the Volksbank <-> Schluss.app <-> Duo Proof of Concept

## Set-up
Use a simple PHP (with SSL) enabled webserver to run the two mockup sites.   

### Volksbank mockup site
Needs to be running at https://localhost:8081 or edit the /volksbank-duo-poc/duo.nl/config.json file and edit the urls accordingly.

### DUO mockup site
Needs to be running at https://localhost:8082 or edit the /volksbank-duo-poc/duo.nl/config.json and /volksbank-duo-poc/volksbank.nl/config.json files and edit the urls accordingly.

### Schluss.app
To test the complete flow you also need to run the schluss.app webapp, which you'll find here: https://gitlab.com/schluss/schluss.app. When those three services are running, start by opening the Volksbank mockup site and scan the displayed QR code with your mobile device or click on it.