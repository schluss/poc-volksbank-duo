<?php

// Example Polling API

header('Content-Type: application/json');

if (!isset($_GET['token']))
{
	echo '{"error":"invalid token"}';
	exit();
}

if (isset($_GET['state']))
{
	echo getState($_GET['token']);
	exit();
}

if (isset($_GET['payload']))
{
	echo getPayload($_GET['token']);
	exit();
}

function getState($token)
{
	$filename = dirname(__FILE__) . '/../store/' . $token . '_state.txt';
	
	if (!file_exists($filename))
		return '';
	
	return file_get_contents($filename);
}

function getPayload($token)
{
	$filename = dirname(__FILE__) . '/../store/' . $token . '_payload.txt';
	
	if (!file_exists($filename))
		return '';
	
	return file_get_contents($filename);
}