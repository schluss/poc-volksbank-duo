<?php

// Example Store/retrieve API with basic PGP encryption

// higher time-limit, decrypting will take some time on some machines!
set_time_limit(120);

// load ext libraries
require_once('../vendor/autoload.php');

header('Content-Type: application/json');

if (!isset($_GET['token']))
{
	echo '{"error":"invalid token"}';
	exit();
}

// when state update
if (isset($_GET['state']))
{
	storeState($_GET['token'], $_GET['state']);
	echo '{"message":"ok"}';
	exit();
}

// when payload is sent by schluss.app
if (isset($_POST['payload']))
{
	// load private key:
	$privKeyData = file_get_contents(dirname(__FILE__) . '/../volksbank_schluss_privkey_pgp.asc');
	$privKeyData = str_replace(chr(0), "", $privKeyData); // bug in file_get_contents does not process the armored ascii string correctly, see: https://www.php.net/manual/en/function.file-get-contents.php#76358

	$PrivKey = OpenPGP_Message::parse(OpenPGP::unarmor($privKeyData, 'PGP PRIVATE KEY BLOCK'));

	// read message
	$message = json_decode($_POST['contract']);
	
	// decrypt contract from message
	$decryptor = new OpenPGP_Crypt_RSA($PrivKey);
	$decrypted = $decryptor->decrypt(OpenPGP::unarmor($message->contract,'PGP MESSAGE'));	
		
	storePayload($_GET['token'], $decrypted->packets[1]->data);
		
	// update the state
	storeState($_POST['token'], 'connected');
		
	echo '{"message":"ok"}';
	exit();
}

function storeState($token, $state)
{
	$filename = dirname(__FILE__) . '/../store/' . $token . '_state.txt';
	$fp = fopen($filename, 'w+');
	fwrite($fp, $state);
	fclose($fp);	
}

function storePayload($token, $payload)
{
	$filename = dirname(__FILE__) . '/../store/' . $token . '_payload.txt';
	$fp = fopen($filename, 'w+');
	fwrite($fp, $payload);
	fclose($fp);
}